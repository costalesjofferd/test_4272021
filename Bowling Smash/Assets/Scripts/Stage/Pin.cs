﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour
{
    [SerializeField] GameObject downIndicator;

    bool isDown = false;

    public Action OnDown;

    void FixedUpdate()
    {
        if(!isDown)
        {
            DetectDown();

            if (isDown)
                OnDown?.Invoke();
            //    downIndicator.SetActive(true);
        }
    }

    void DetectDown()
    {
        float dot = Vector3.Dot(Vector3.up, transform.up);

        if (dot < 0.8f)
            isDown = true;
    }
}
