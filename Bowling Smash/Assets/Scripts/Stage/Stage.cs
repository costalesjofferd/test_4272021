﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    [SerializeField] GameObject[] stageWaves;
    [SerializeField] Transform goal;
    [SerializeField] Road mainRoad;
    [SerializeField] float spacePerWave = 2f;

    public int ballWavesCount;

    Player _player;
    private void Start()
    {
        PositionWaves();
        SetRoadLenght();
        SetGoalPosition();

        InitializeWaves();
        InitializePlayer();
    }

    void InitializePlayer()
    {
        _player = FindObjectOfType<Player>();

        if (_player == null)
            return;

        Color color = Color.red;

        IColorSource colorSource = stageWaves[0].GetComponent<IColorSource>();
        if (colorSource != null)
            color = colorSource.GetColor();

        _player.SetColor(color);
    }

    void PositionWaves()
    {
        for(int i = 0; i < stageWaves.Length; i++ )
        {
            Vector3 pos = new Vector3(0, 0, spacePerWave * (i+1));

            stageWaves[i].transform.position = pos;
        }
    }

    void SetRoadLenght()
    {
        mainRoad.SetLenght((stageWaves.Length + 1) * spacePerWave);
    }

    void SetGoalPosition()
    {
        goal.position = new Vector3(0, 0, (stageWaves.Length + 1) * spacePerWave);
    }


    public void InitializeWaves()
    {
        for(int i = 0; i < stageWaves.Length; i ++)
        {
            GameObject nextWave = null;
            int nextIndex = i + 1;

            if (nextIndex < stageWaves.Length)
                nextWave = stageWaves[nextIndex];

            stageWaves[i].GetComponent<IStageWave>()?.Initialize(nextWave);
        }
    }
   

    private void OnValidate()
    {
        ValidateStageWaves();
        CountBallWaves();
    }

    void ValidateStageWaves()
    {
        if (stageWaves == null)
            return;

        for (int i = 0; i < stageWaves.Length; i++)
        {
            if (stageWaves[i] == null)
                continue;

            if (stageWaves[i].GetComponent<IStageWave>() == null)
                Debug.LogError("Stage wave at index " + i + " does not implement IStageWave");
        }
    }

    void CountBallWaves()
    {
        ballWavesCount = 0;
        for (int i = 0; i < stageWaves.Length; i++)
        {
            if (stageWaves[i] != null && stageWaves[i].GetComponent<BallWave>() != null)
                ballWavesCount++;
        }
    }
}
