﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallWave : MonoBehaviour, IStageWave, IColorSource
{
    public Color[] colors = new Color[3];
    [SerializeField] BallPickup ballPrefab;
    [SerializeField] float spacing;


    public int[] outIds;


    int spawnCount = 3;

    public void Initialize(GameObject nextWave)
    {
        SpawnColoredBalls();
    }

    public void SpawnColoredBalls()
    {
        List<int> temp = new List<int>(spawnCount);

        for(int i = 0; i < spawnCount; i ++)
        {
            temp.Add(i);
        }

        outIds = new int[spawnCount];

        int currentIndex = 0;

        while(temp.Count > 0)
        {
            int random = UnityEngine.Random.Range(0, temp.Count);
            int id = temp[random];
            temp.Remove(id);
            outIds[currentIndex] = id;
            currentIndex++;
        }

        for(int i = 0; i < outIds.Length; i ++)
        {
            BallPickup ball = Instantiate(ballPrefab, transform);
            int colorIndex = outIds[i];
            ball.SetColor(colors[colorIndex]);

            float x = (((spawnCount - 1) / -2f) + i) * spacing;
            ball.transform.position = new Vector3(x, ball.transform.position.y, ball.transform.position.z);
        }
    }

    public Color GetColor()
    {
        int random = UnityEngine.Random.Range(0, colors.Length);
        return colors[random];
    }
}
