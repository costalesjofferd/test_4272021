﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public int totalScore;

    private void Start()
    {
        Pin[] pins = FindObjectsOfType<Pin>();

        foreach (Pin pin in pins)
            pin.OnDown += OnPinDown;
    }


    [SerializeField] float victoryDelay = 6f;
    [SerializeField] float panDelay = 3f;

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();

        if (player != null)
        {
            player.EndRun();
            player.StartKick();

            PowerBarUI powerBarUI = FindObjectOfType<PowerBarUI>();
            powerBarUI.AnimateArrow();

            Invoke("PanToCenter", panDelay);
            Invoke("Victory", victoryDelay);
        }

        CameraControl cameraControl = FindObjectOfType<CameraControl>();
        cameraControl.PanToRight();
    }

    void PanToCenter()
    {
        CameraControl cameraControl = FindObjectOfType<CameraControl>();
        cameraControl.PanToCenter();
    }

    void Victory()
    {
        VictoryScreen victoryScreen = FindObjectOfType<VictoryScreen>();
        victoryScreen.ShowScore(totalScore);
    }

    private void OnPinDown()
    {
        totalScore++;
    }
}
