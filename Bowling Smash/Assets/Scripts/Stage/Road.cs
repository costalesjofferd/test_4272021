﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Road : MonoBehaviour
{
    private static Road activeRoad;
    public static Action<Road> OnActiveRoadChanged;

    [SerializeField] BoxCollider _boxCollider;
    [SerializeField] float borderWidth = 0.1f;
    [SerializeField] float extraLenght = 5f;
    [SerializeField] bool autoActive = false;

    public float minX { get; private set; }
    public float maxX { get; private set; }
    public float width { get; private set; }

    private void Awake()
    {
        if(autoActive)
            SetAsActiveRoad();
    }

    private void Start()
    {
        ComputeBounds();
    }

    public void SetLenght(float lenght)
    {
        transform.localScale = new Vector3(1, 1, lenght + extraLenght);
        transform.position = new Vector3(0, 0, -extraLenght);
    }

    public void SetAsActiveRoad()
    {
        activeRoad = this;
        OnActiveRoadChanged?.Invoke(activeRoad);
    }

    public static Road GetActiveRoad()
    {
        return activeRoad;
    }

    private void OnValidate()
    {
        ComputeBounds();
    }

    void ComputeBounds()
    {
        minX = _boxCollider.bounds.min.x + borderWidth;
        maxX = _boxCollider.bounds.max.x - borderWidth;
        width = maxX - minX;
    }

    private void OnDrawGizmosSelected()
    {
        float minZ = _boxCollider.bounds.min.z;
        float maxZ = _boxCollider.bounds.max.z;

        Vector3 l1_Start = new Vector3(minX, 0, minZ);
        Vector3 l1_End = new Vector3(minX, 0, maxZ);


        Vector3 l2_Start = new Vector3(maxX, 0, minZ);
        Vector3 l2_End = new Vector3(maxX, 0, maxZ);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(l1_Start, l1_End);
        Gizmos.DrawLine(l2_Start, l2_End);
    }
}
