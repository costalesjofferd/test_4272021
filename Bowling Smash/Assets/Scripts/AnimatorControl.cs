﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatorControl : MonoBehaviour
{
    [SerializeField] private RuntimeAnimatorController idleController;
    [SerializeField] private RuntimeAnimatorController runController;
    [SerializeField] private RuntimeAnimatorController kickController;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Idle()
    {
        SetController(idleController);
    }

    public void Run()
    {
        SetController(runController);
    }

    public void Kick()
    {
        SetController(kickController);
    }

    void SetController(RuntimeAnimatorController controller)
    {
        _animator.runtimeAnimatorController = controller;
    }
}
