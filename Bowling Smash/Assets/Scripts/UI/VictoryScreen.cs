﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VictoryScreen : MonoBehaviour
{
    [SerializeField] GameObject root;
    [SerializeField] Text scoreText;
    [SerializeField] Button nextLevelButton;

    static int maxStage = 5;
    private void Start()
    {
        nextLevelButton.onClick.AddListener(NextLevel);
    }

    public void ShowScore(int score)
    {
        scoreText.text = score.ToString();
        root.gameObject.SetActive(true);
    }

    void NextLevel()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        sceneIndex++;

        if (sceneIndex >= maxStage)
            sceneIndex = 0;

        SceneManager.LoadScene(sceneIndex);
    }
}
