﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangeWave : MonoBehaviour, IStageWave, IColorSource
{
    [SerializeField] Color color;
    [SerializeField] ColorWall colorWallPrefab;

    public Color GetColor()
    {
        return color;
    }

    public void Initialize(GameObject nextWave)
    {
        if (nextWave == null)
            return;

        BallWave ballWave = nextWave.GetComponent<BallWave>();

        if (ballWave == null)
            return;

        color = ballWave.GetColor();

        ColorWall colorWall = Instantiate(colorWallPrefab, transform);
        colorWall.SetColor(color);
    }
}
