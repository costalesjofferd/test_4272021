﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStageWave
{
    void Initialize(GameObject nextWave);
}
