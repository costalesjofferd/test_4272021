﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IColorable
{
    public Ball ball;

    [SerializeField] Rigidbody rigidbody;
    [SerializeField] AnimatorControl animatorControl;

    [SerializeField] AnimationCurve magnitudeCurve;
    [SerializeField] float maxKickMagnitude;
    [SerializeField] float kickMagnitude;

    [SerializeField] [Range(0, 100)] float runSpeed;
    public float kickDelay = 1f;

    [SerializeField] Renderer[] _renderers;
    MaterialPropertyBlock _propBlock;

    OnScreenAnalog _onScreenAnalog;
    Road _activeRoad;

    public Color color { get; private set; }
    public bool hasControl;

    public Action<float> OnCompletionChanged;

    int points = 0;
    int maxPoints;

    private void Start()
    {
        _onScreenAnalog = FindObjectOfType<OnScreenAnalog>();

        if (_onScreenAnalog)
            _onScreenAnalog.OnDrag += OnDrag;

        Road.OnActiveRoadChanged += OnActiveRoadChanged;
        OnActiveRoadChanged(Road.GetActiveRoad());

        animatorControl.Idle();

        Stage stage = FindObjectOfType<Stage>();
        maxPoints = stage.ballWavesCount;
    }

    void OnActiveRoadChanged(Road newActive)
    {
        _activeRoad = newActive;
    }

    private void Update()
    {
        if (!hasControl)
            return;

        UpdateConstantVelocity();
    }

    private void FixedUpdate()
    {
        if (!hasControl)
            StopRigibody();
    }

    void OnDrag(float deltaX)
    {
        if (!hasControl)
            return;

        transform.position += new Vector3(deltaX, 0f);
        ClampToRoad();
    }

    public void StartRun()
    {
        animatorControl.Run();
        hasControl = true;
        ball.StarRoll();
    }

    public void EndRun()
    {
        animatorControl.Idle();
        ball.StopRoll();
        hasControl = false;
    }

    public void StartKick()
    {
        animatorControl.Kick();
        Invoke("Kick", kickDelay);
    }

    public void SetKickMagnitude(float rate)
    {
        //from min to max
        float curve = magnitudeCurve.Evaluate(rate);
        kickMagnitude = maxKickMagnitude * curve;
    }

    void Kick()
    {
        ball.Kick(kickMagnitude);
    }

    public void OnPickUp(Color color)
    {
        if(this.color == color)
        {
            ball.IncreaseSize();

            points = Mathf.Clamp(points + 1, 0, maxPoints);
        }
        else
        {
            ball.DecreaseSize();

            points = Mathf.Clamp(points - 1, 0, maxPoints);
        }

        OnCompletionChanged?.Invoke(points / (float)maxPoints);
    }

    public void SetColor(Color color)
    {
        this.color = color;
        _propBlock = new MaterialPropertyBlock();
        _renderers[0].GetPropertyBlock(_propBlock);
        _propBlock.SetColor("_Color", color);

        for(int i =0; i < _renderers.Length; i ++)
            _renderers[i].SetPropertyBlock(_propBlock);

        ball.SetColor(color);
    }

    void UpdateConstantVelocity()
    {
        rigidbody.velocity = Vector3.forward * runSpeed;
    }

    void StopRigibody()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }

    void ClampToRoad()
    {
        float x = transform.position.x;
        
        if (_activeRoad != null)
            x = Mathf.Clamp(x, _activeRoad.minX, _activeRoad.maxX);

        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }
}
