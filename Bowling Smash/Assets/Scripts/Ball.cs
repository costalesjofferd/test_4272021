﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour, IColorable
{
    [SerializeField] Rigidbody _rigidbody;

    [SerializeField] [Range(0, 100)] float rollForce;

    [SerializeField] Transform pivotTransform;
    [SerializeField] Vector3 offset;

    [SerializeField] Renderer _renderer;

    public bool isRolling { get; private set; }

    MaterialPropertyBlock _propBlock;

    public float targetSize;
    float lerp = 0.1f;


    [SerializeField] float initialRadius = 1;
    [SerializeField] float minRadius;
    [SerializeField] float maxRadius;
    [SerializeField] float growPerPoint;

    private void Start()
    {
        targetSize = initialRadius;
    }

    private void Update()
    {
        if(isRolling)
        {
            Roll();

            GrowToTargetSize();
            UpdatePositionToPivot();
        }
    }

    private void OnValidate()
    {
        if (pivotTransform == null)
            return;

        offset = transform.position - pivotTransform.position;
    }

    void GrowToTargetSize()
    {
        Vector3 scale = transform.localScale;
        float r = scale.x;

        float n = Mathf.Lerp(r, targetSize, lerp);
        transform.localScale = new Vector3(n, n, n);
    }

    public void SetColor(Color color)
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer.GetPropertyBlock(_propBlock, 0);
        _propBlock.SetColor("_Color", color);
        _renderer.SetPropertyBlock(_propBlock, 0);
    }

    public void IncreaseSize()
    {
        targetSize = Mathf.Clamp(targetSize + growPerPoint, minRadius, maxRadius);
    }

    public void DecreaseSize()
    {
        targetSize = Mathf.Clamp(targetSize - growPerPoint, minRadius, maxRadius);
    }

    void UpdatePositionToPivot()
    {
        if (pivotTransform == null)
            return;

        transform.position = pivotTransform.position + (offset * transform.localScale.x);
    }


    public void StarRoll()
    {
        isRolling = true;
    }

    public void StopRoll()
    {
        isRolling = false;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = _rigidbody.angularVelocity * 0.6f;
    }

    void Roll()
    {
        _rigidbody.velocity = Vector3.forward * rollForce;
    }

    public void Kick(float magnitude)
    {
        _rigidbody.AddForce(Vector3.forward * magnitude * 100);
    }

    public void OnSizeChange()
    {
    }
}
