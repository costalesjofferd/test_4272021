﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPickup : MonoBehaviour, IColorable
{
    [SerializeField] Renderer _renderer;
    private MaterialPropertyBlock _propBlock;

    Color _color;
    private void Start()
    {
    }

    public void SetColor(Color color)
    {
        _color = color;
        _propBlock = new MaterialPropertyBlock();
        _renderer.GetPropertyBlock(_propBlock, 0);
        _propBlock.SetColor("_Color", color);
        _renderer.SetPropertyBlock(_propBlock, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();

        if(player != null)
        {
            player.OnPickUp(_color);

            if (player.color == _color)
                Destroy(gameObject);
        }
    }

}
