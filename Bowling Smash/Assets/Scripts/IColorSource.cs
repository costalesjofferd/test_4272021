﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColorSource
{
    Color GetColor();
}
