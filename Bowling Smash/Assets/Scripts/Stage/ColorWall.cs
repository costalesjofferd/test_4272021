﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorWall : MonoBehaviour, IColorable
{
    public Color color;
    [SerializeField] ParticleSystem particleSystem;

    public void SetColor(Color color)
    {
        this.color = color;
        var main = particleSystem.main;
        main.startColor = new ParticleSystem.MinMaxGradient(color, color);
    }

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();

        if (player != null)
        {
            player.SetColor(color);
        }
    }
}
