﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayMenu : MonoBehaviour
{
    [SerializeField] Button goButton;
    [SerializeField] GameObject swipeTip;
    [SerializeField] PowerBarUI powerBarUI;

    Player _player;
    OnScreenAnalog _onScreenAnalog;

    private void Start()
    {
        _player = FindObjectOfType<Player>();
        _onScreenAnalog = FindObjectOfType<OnScreenAnalog>();
        _onScreenAnalog.OnFirstTouch += OnFirstTouch;

        swipeTip.SetActive(false);
        goButton.onClick.AddListener(GoOnClick);
    }

    void GoOnClick()
    {
        _player.StartRun();
        _onScreenAnalog.hasControl = true;
        powerBarUI.gameObject.SetActive(true);

        swipeTip.SetActive(true);

        goButton.gameObject.SetActive(false);
    }

    void OnFirstTouch()
    {
        swipeTip.SetActive(false);
    }
}
