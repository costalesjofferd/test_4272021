﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] Transform targetTransform;
    public Vector3 offset;


    public bool trackX = true;
    public bool trackY = true;
    public bool trackZ = true;

    [SerializeField] Vector3 originalPan;
    [SerializeField] Vector3 rightPan;
    [SerializeField] Vector3 centerPan;
    [SerializeField] float panningSpeed = 1f;

    private void OnValidate()
    {
        if (targetTransform == null)
            return;

        offset = transform.position - targetTransform.position;
    }

    private void Update()
    {
        if (targetTransform == null)
            return;

        Vector3 trackedPos = targetTransform.position + offset;
        float x = trackX ? trackedPos.x : transform.position.x;
        float y = trackY ? trackedPos.y : transform.position.y;
        float z = trackZ ? trackedPos.z : transform.position.z;

        transform.position = new Vector3(x,y,z);
    }

    public Vector3 targetPan;
    public Vector3 eulerAngles;

    public void PanToRight()
    {
        targetPan = rightPan;
        StartCoroutine(PanToTarget());
    }

    public void PanToCenter()
    {
        targetPan = centerPan;
        StartCoroutine(PanToTarget());
    }

    public void PanToOriginal()
    {
        targetPan = originalPan;
        StartCoroutine(PanToTarget());
    }

    IEnumerator PanToTarget()
    {
        Quaternion q = transform.rotation;
        Quaternion q2 = Quaternion.identity;

        while (true)
        {
            q2.eulerAngles = targetPan;
            q = Quaternion.Lerp(q, q2, panningSpeed * Time.deltaTime);
            transform.rotation = q;

            if (q2.eulerAngles == q.eulerAngles)
                yield break;

            yield return null;
        }
    }
}
