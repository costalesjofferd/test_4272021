﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnScreenAnalog : MonoBehaviour
{
    private Vector2 mouseOriginPos;
    private Vector2 previousPos;

    [SerializeField] float dragMultiplier = 2f;
    public float dragDelta { get; private set; }
    public bool hasTouch { get; private set; }


    public Action<float> OnDrag;
    public Action OnFirstTouch;

    public bool hasControl;
    private bool hasFirstTouch;

    private void Update()
    {
        if (!hasControl)
            return;

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            hasTouch = true;
            RecordTouchOrigin();

            if(!hasFirstTouch)
            {
                OnFirstTouch?.Invoke();
                hasFirstTouch = true;
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            hasTouch = false;
        }

        ComputeDragDelta();
    }

    void RecordTouchOrigin()
    {
        mouseOriginPos = Input.mousePosition;
        previousPos = mouseOriginPos;
    }

    void ComputeDragDelta()
    {
        if(hasTouch)
        {
            Vector2 newPos = Input.mousePosition;
            Vector2 delta = newPos - previousPos;

            Vector2 screenSize = new Vector2(Screen.width, Screen.height);

            dragDelta = delta.x / screenSize.x;
            dragDelta *= dragMultiplier;
            OnDrag?.Invoke(dragDelta);

            previousPos = newPos;
        }
        else
        {
            if (dragDelta != 0)
            {
                dragDelta = 0;
                OnDrag?.Invoke(0);
            }
        }
    }
}
