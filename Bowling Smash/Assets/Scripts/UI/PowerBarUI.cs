﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerBarUI : MonoBehaviour
{
    
    [SerializeField] float minValue = 0.2f;
    [SerializeField] Image fillImage;
    [SerializeField] RectTransform arrow;
    [SerializeField] float arrowSpeed = 20f;
    [SerializeField] float barMin = 20;
    [SerializeField] float autoStopTime = 6f;
    [SerializeField] bool stopped = false;

    private RectTransform myRect;
    public float rectHeight;
    public float arrowHeight;

    float maxVal;


    Player _player;
    private void Start()
    {
        myRect = GetComponent<RectTransform>();
        rectHeight = myRect.sizeDelta.y;
        arrowHeight = arrow.sizeDelta.y;

        _player = FindObjectOfType<Player>();

        if (_player == null)
            return;

        _player.OnCompletionChanged += SetPoints;

        SetPoints(0);
    }

    public void SetPoints(float completion)
    {
        float rate = (1 - minValue) * completion;

        float barRate = rate + minValue;
        fillImage.fillAmount = barRate;
        maxVal = barRate;
    }

    public void AnimateArrow()
    {
        stopped = false;
        StartCoroutine(IEAnimateArrow());
    }

    public void StopArrow()
    {
        stopped = true;
    }

    IEnumerator IEAnimateArrow()
    {
        float minY = barMin;
        float maxY = Mathf.Clamp( (rectHeight * maxVal), minY, rectHeight);


        float direction = 1f;//up
        float remainingTime = autoStopTime;

        arrow.gameObject.SetActive(true);

        while (!stopped)//wait for press
        {
            float yPos = arrow.anchoredPosition.y;

            if (direction > 0)//up
            {
                yPos += arrowSpeed * Time.deltaTime;

                if(yPos > maxY)
                    direction = -1;
            }
            else
            {
                yPos -= arrowSpeed * Time.deltaTime;

                if (yPos < minY)
                    direction = 1;
            }

            yPos = Mathf.Clamp(yPos, minY, maxY);
            arrow.anchoredPosition = new Vector2(arrow.anchoredPosition.x, yPos);

            remainingTime -= Time.deltaTime;

            if (remainingTime <= 0)
                stopped = true;

            if (Input.GetKeyDown(KeyCode.Mouse0))
                stopped = true;

            yield return null;
        }

        float magnitude = (arrow.anchoredPosition.y - minY) / (rectHeight - minY);
        _player.SetKickMagnitude(magnitude);

        yield return new WaitForSeconds(remainingTime + 2f);
        gameObject.SetActive(false);
    }
}
